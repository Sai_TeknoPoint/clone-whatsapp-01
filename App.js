import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Header from './screens/Header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import NavComp from './screens/NavComp';
// import MainComponents from '../ChatApp/screens/MainComponents'
import SingleChatScreen from '../clone-whatsapp-01/screens/SingleChatScreen'
import LoginScreen from '../clone-whatsapp-01/screens/LoginScreen';
import Otp from '../clone-whatsapp-01/screens/Otp';


const Stack = createStackNavigator();


export default function App(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='LoginScreen' screenOptions={{ headerShown: false }}>
        <Stack.Screen name='LoginScreen' component={LoginScreen}></Stack.Screen>
        <Stack.Screen name='Otp' component={Otp}></Stack.Screen>
        <Stack.Screen name='Header' component={Header}></Stack.Screen>
        {/* <Stack.Screen name='NavComp' component={NavComp}></Stack.Screen> */}
        <Stack.Screen name='SingleChatScreen' component={SingleChatScreen}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>


    // <SafeAreaView style={styles.container}>

    //     <Header />
    //     {/* <NavComp></NavComp> */}

    // </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',

  },
});
