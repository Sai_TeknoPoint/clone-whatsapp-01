import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, SafeAreaView, NativeModules } from 'react-native';
import CameraIcon from 'react-native-vector-icons/Feather'
import SearchIcon from 'react-native-vector-icons/EvilIcons'
import VerticalDots from 'react-native-vector-icons/Entypo'
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import { PERMISSIONS, RESULTS, request, check } from 'react-native-permissions';
import NavComp from './NavComp';
import SingleChatScreen from '../screens/SingleChatScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MainChat from './TabNavComp/MainChat';
import Status from './TabNavComp/Status'
import Calls from './TabNavComp/Calls'
import { useNavigation } from '@react-navigation/native';



const { CalendarModule } = NativeModules;




const Header = () => {
  const apiKey = "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";
  const model = "text-davinci-003";
  const prompt = "Convert this text to a programmatic command:\n\nExample: Ask Constance if we need some bread\nOutput: send-msg `find constance` Do we need some bread?\n\nReach out to the ski store and figure out if I can get my skis fixed before I leave on Thursday";
  const temperature = 0;
  const maxTokens = 100;
  const topP = 1.0;
  const frequencyPenalty = 0.2;
  const presencePenalty = 0.0;
  const stop = ["\n"];

  async function getCompletion() {
    const response = await fetch(
      `https://api.openai.com/v1/engines/text-davinci-003/completions`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${apiKey}`
        },
        body: JSON.stringify({
          prompt,
          temperature,
          max_tokens: maxTokens,
          top_p: topP,
          frequency_penalty: frequencyPenalty,
          presence_penalty: presencePenalty,
          stop
        })
      }
    );
    const json = await response.json();
    // console.log(json, "reponsssssssssssssssssssssssssssssssssssssssssssssssssssssss");
    return json.choices[0].text;
  }

  function myCallBack(valueText) {
    console.log("valueText from java-------", valueText);

  }
  const onPress = () => {
    CalendarModule.createCalendarEvent('testName', 'testLocation', myCallBack);
  };

  useEffect(() => {
    getCompletion()
  }, [])

  const navigation = useNavigation();


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container} >
        <View>
          <Text style={styles.Text1} >ChatsApp</Text>
        </View>
        <View style={styles.iconsContainer}>
          <TouchableOpacity
            onPress={onPress}
          >
            <CameraIcon name='camera' size={25} color="white" ></CameraIcon>
          </TouchableOpacity>
          <TouchableOpacity>
            <SearchIcon name='search' size={30} color='white' ></SearchIcon>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { navigation.navigate("SingleChatScreen") }} >
            <VerticalDots name='dots-three-vertical' size={25} color='white' ></VerticalDots>
          </TouchableOpacity>
        </View>
      </View>
      <NavComp></NavComp>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#006D5B',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: "10%",
    flexDirection: 'row',
    padding: 15
  },
  Text1: {
    color: "white",
    fontSize: 23,
    fontWeight: 'bold'
  },
  iconsContainer: {
    flexDirection: 'row',
    // backgroundColor:"red",
    width: "35%",
    alignItems: "center",
    justifyContent: "space-between"
  }
});


export default Header