import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Keyboard, Alert } from 'react-native';
import auth from '@react-native-firebase/auth';

const LoginScreen = ({ navigation }) => {
  const [countryCode, setCountryCode] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  const sendOTP = async () => {
    try {
      const phoneNumber = `+${countryCode}${mobileNumber}`;
      const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
      // Here, `confirmation` is an object that represents the OTP verification process.
      // You can store this object and use it later for OTP verification.
      console.log(confirmation, "confirmation");

      Alert.alert('OTP Sent', 'An OTP has been sent to your phone number.');

      // Proceed with OTP verification or handle the confirmation object as needed.
    } catch (error) {
      console.log('Error sending OTP:', error);
      Alert.alert('Error', 'Failed to send OTP. Please try again later.');
    }
  };

  const handleMobileNumberSubmit = () => {
    // Validation
    if (countryCode.trim().length === 0 || mobileNumber.trim().length === 0) {
      Alert.alert('Please enter both the country code and mobile number.');
      return;
    }

    if (!/^\d{10}$/.test(mobileNumber)) {
      Alert.alert('Please enter a valid 10-digit mobile number.');
      return;
    }

    // Logic to handle mobile number submission
    const fullMobileNumber = `+${countryCode}${mobileNumber}`;
    navigation.navigate('Otp', { mobileNumber: fullMobileNumber });
  };

  const handleMobileNumberChange = (text) => {
    setMobileNumber(text);

    if (text.length === 10) {
      Keyboard.dismiss();
    }
  };

  const handleCountryCodeChange = (text) => {
    setCountryCode(text);

    if (text.length === 2) {
      Keyboard.dismiss();
    } else if (text.length > 2) {
      Alert.alert("Country code cannot be greater than 2 numbers");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>ChatsApp</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.countryCodeinput}
          placeholder="91"
          value={countryCode}
          onChangeText={handleCountryCodeChange}
          keyboardType="numeric"
          onSubmitEditing={handleCountryCodeChange}
        />
        <TextInput
          style={styles.input}
          placeholder="Mobile Number"
          value={mobileNumber}
          onChangeText={handleMobileNumberChange}
          keyboardType="numeric"
          onSubmitEditing={handleMobileNumberSubmit}
        />
      </View>
      <Button title="Next" onPress={sendOTP} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 32,
  },
  inputContainer: {
    flexDirection: 'row',
    marginBottom: 16,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginRight: 10,
  },
  countryCodeinput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginRight: 10,
    width: "10%"
  },
});

export default LoginScreen;
