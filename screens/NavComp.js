import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainChat from './TabNavComp/MainChat';
import Status from './TabNavComp/Status'
import Calls from './TabNavComp/Calls'
import { StyleSheet, Text,TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import SingleChatScreen from './SingleChatScreen'
import Header from './Header';



// const Stack = createNativeStackNavigator();
const Tab = createMaterialTopTabNavigator();


function Chats() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
      <MainChat  ></MainChat>
    </View>
  );
}

function StatusComp() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
      <Status></Status>
    </View>
  );
}

function CallsComp() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
      <Calls></Calls>
    </View>
  );
}

const NavComp = () => {
  const navigation = useNavigation();

  return (
    <>
    {/* <NavigationContainer initialRouteName='Status' independent={true}> */}
      <Tab.Navigator>
        <Tab.Screen name="Chats"
          component={Chats}
          options={{
            // tabBarLabel: 'Chats',
            // tabBarIcon: ({ color }) => (
            //   // <MaterialCommunityIcons name="home" color={color} size={26} />
            //   <Icon name='ticket' size={25} color={color} ></Icon>
            // ),
            tabBarStyle: { backgroundColor:'#006D5B' },
            tabBarLabelStyle: {color:"white", },
          }}
          
        />
        <Tab.Screen name="Status"
          component={StatusComp}
          options={{
            // tabBarLabel: 'Chats',
            // tabBarIcon: ({ color }) => (
            //   // <MaterialCommunityIcons name="home" color={color} size={26} />
            //   <Icon name='ticket' size={25} color={color} ></Icon>
            // ),
            tabBarStyle: { backgroundColor:'#006D5B' },
            tabBarLabelStyle: {color:"white", },
          }}
        />
        <Tab.Screen name="Calls"
          component={CallsComp}
          options={{
            // tabBarLabel: 'Chats',
            // tabBarIcon: ({ color }) => (
            //   // <MaterialCommunityIcons name="home" color={color} size={26} />
            //   <Icon name='ticket' size={25} color={color} ></Icon>
            // ),
            tabBarStyle: { backgroundColor:'#006D5B' },
            tabBarLabelStyle: {color:"white", },
          }}
        />
      </Tab.Navigator>
    {/* </NavigationContainer> */}
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});


export default NavComp