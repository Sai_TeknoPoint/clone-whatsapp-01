import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';


const Otp = ({ route }) => {
    const [otp, setOtp] = useState('');
    let [OTP, setOTP] = useState('');

    const [error, setError] = useState('');
    const MobileNumber = route.params.mobileNumber

    const handleOtpSubmit = () => {
        if (otp.trim().length === 0) {
            setError('Please enter the OTP.');
        } else if (otp.trim().length !== 6) {
            setError('OTP should be 6 digits long.');
        } else {
            setError('');
            console.log('OTP:', otp);
        }
    };

    const regexOtp = /^[0-9]{6}$/;
    const validateOTP = () => {
        console.log("working");
        if (regexOtp.test(OTP)) {
            if (Global.userType == '') {
                navigation.navigate("AddPatient");
            }
            else {
                navigation.navigate("Dashboard");
            }
        }
        else {
            if (!isNaN(OTP)) {
                setError(false)
            }
            setError(true)
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.heading}>OTP Verification</Text>
            <Text style={styles.subheading}>
                Please enter the OTP sent to this mobile number {MobileNumber}.
            </Text>
            {/* <TextInput
                style={styles.input}
                placeholder="OTP"
                value={otp}
                onChangeText={(text) => setOtp(text)}
                keyboardType="numeric"
            /> */}
            <View style={{ flex: 1 }}>
                <OTPInputView
                    autoFocusOnLoad
                    pinCount={4}
                    code={OTP}
                    onCodeChanged={val => {
                        setOTP(val);
                        if (val.length === 6) {
                            setError(false);
                        }
                    }}
                    keyboardType={'number-pad'}
                    codeInputFieldStyle={{
                        borderRadius: 14,
                        backgroundColor: "white",
                        marginHorizontal:25, // Adjust this value to decrease spacing
                    }}
                    editable={true}
                />
            </View>
            {error ? <Text style={styles.errorText}>{error}</Text> : null}
            <Button title="Verify" onPress={validateOTP} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 16,
    },
    heading: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 32,
    },
    subheading: {
        fontSize: 16,
        marginBottom: 16,
        textAlign: 'center',
    },
    input: {
        width: '100%',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 16,
        paddingHorizontal: 10,
    },
    errorText: {
        color: 'red',
        marginBottom: 16,
        textAlign: 'center',
    },
});

export default Otp;
