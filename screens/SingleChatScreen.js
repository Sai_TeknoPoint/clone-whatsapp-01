import React,{useState} from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image, FlatList, Modal, Pressable,TextInput,NativeModules } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import VideoCallIcon from 'react-native-vector-icons/FontAwesome'
import CallIcon from 'react-native-vector-icons/MaterialIcons'
import ThreeDots from 'react-native-vector-icons/Entypo'
import BackArrow from 'react-native-vector-icons/Ionicons'
import io from 'socket.io-client';

const socket = io('http://localhost:8080');

// const {CalendarModule} = NativeModules;

const ChatInput = ({ onSend }) => {
  const [message, setMessage] = useState("");

  const handleMessage = (text) => {
    setMessage(text);
  };

  const handleSend = () => {
    onSend(message);
    setMessage("");
  };

  return (
    <View style={styles.inputContainer}>
      <TextInput
        style={styles.input}
        placeholder="Type a message"
        value={message}
        onChangeText={handleMessage}
      />
      <Text style={styles.sendButton} onPress={handleSend}>Send</Text>
    </View>
  );
};

const ChatDisplay = ({ messages }) => {
  return (
    <View style={styles.chatContainer}>
      {messages.map((message, index) => (
        <View
          key={index}
          style={[
            styles.messageContainer,
            message.sender === "me" ? styles.myMessage : styles.otherMessage,
          ]}
        >
          <Text style={styles.messageText}>{message.text}</Text>
        </View>
      ))}
    </View>
  );
};

const SingleChatScreen = ({ route }) => {
  const navigation = useNavigation();
  const [messages, setMessages] = useState([]);
  

  // function myCallBack(valueText){
  //   console.log("valueText from java-------",valueText);

  // }
  // const onPress = () => {
  //   CalendarModule.createCalendarEvent('testName', 'testLocation',myCallBack);
  // };


  let passedData = route.params.item;

  const handleSend = (message) => {
    setMessages([...messages, { text: message, sender: "me" }]);
  };

  // Send a message
socket.emit('message', {
  sender: 'user123',
  recipient: 'user456',
  text: 'Hello, world!',
});

// Receive a message
socket.on('message', function(message) {
  console.log('Received message:', message);
});

  return (
    <View style={{flex:1}}>
      <View style={{ backgroundColor: "#006D5B", height: 75, }} >
        <View style={{ alignItems:"center",flexDirection:"row",justifyContent:"space-between" }} >
          {/* back arrow icon here */}
          <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",}}>
            <TouchableOpacity>
          <BackArrow name='arrow-back' size={25} color="white" ></BackArrow>
            </TouchableOpacity>
            <TouchableOpacity>
          <Image source={{ uri: passedData?.item?.image }} style={{ width: 70, height: 70, borderRadius: 50,marginBottom:13 }} resizeMode='cover' ></Image>
            </TouchableOpacity>
            <TouchableOpacity>
            <Text style={{ color: "white",fontSize:20, }}>{passedData?.item?.firstName}</Text>
            <Text style={{ color: "white", marginLeft: 8,fontSize:20,marginRight:10 }}>{passedData?.item?.lastName}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ alignItems: "center", flexDirection: "row",justifyContent:"space-between",width:125,marginRight:15}}>
          <VideoCallIcon name='video-camera' size={25} color="white" ></VideoCallIcon>
          <CallIcon name='call' size={25} color="white" ></CallIcon>
          <ThreeDots name='dots-three-vertical' size={25} color="white" ></ThreeDots>
          </View>
          
        </View>
      </View>
      <ChatDisplay messages={messages} />
      <ChatInput onSend={handleSend} />
    </View>
  )
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    
  },
  input: {
    flex: 1,
    marginRight: 10,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
  },
  sendButton: {
    color: "#006D5B",
    fontWeight: "bold",
    fontSize: 16,
  },
  chatContainer: {
    flex: 1,
    padding: 10,
  },
  messageContainer: {
    maxWidth: "80%",
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
  },
  myMessage: {
    alignSelf: "flex-end",
    backgroundColor: "#006D5B",
  },
  otherMessage: {
    alignSelf: "flex-start",
    backgroundColor: "white",
  },
  messageText: {
    color: "black",
    fontSize: 16,
  },
});

export default SingleChatScreen;
