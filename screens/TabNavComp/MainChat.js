import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image, FlatList, Modal, Pressable } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo'
import SecondIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import ThirdIcon from 'react-native-vector-icons/Ionicons'
import FourthIcon from 'react-native-vector-icons/FontAwesome'
import FifthIcon from 'react-native-vector-icons/Feather'
import { BlurView } from "@react-native-community/blur";
import { useNavigation } from '@react-navigation/native';
import SingleChatScreen from '../SingleChatScreen'





const MainChat = (props) => {

  let [data, setData] = useState([])
  let [modalstatus, setmodalstatus] = useState(false);
  let [selectedUserData, setselectedUserData] = useState({});
  const navigation = useNavigation();
  useEffect(() => {
    
    fetch('https://dummyjson.com/users')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error, "error------------------"))
  }, [])



  return (
    <View style={{ width: "100%", padding: 10, height: "100%" }}>
      <FlatList
        data={data.users}
        keyExtractor={(item, index) => "key" + index}
        renderItem={(item) => {
          return (
            <TouchableOpacity onPress={() => {navigation.navigate("SingleChatScreen",{item:item})}} style={{ marginBottom: "4%", justifyContent: "center", flexDirection: "row" }}>
              {/* {console.log(item.item.image, "item------------")} */}
              <TouchableOpacity onPress={() => {
                // const user = {
                //   name:item.
                // }
                
                setselectedUserData(item);
                setmodalstatus(!modalstatus);
              }} style={{ height: 75, width: 75, justifyContent: "center", alignItems: "center", }}>
                <Image style={{ height: 60, width: 60, }} source={{ uri: item.item.image }}></Image>
              </TouchableOpacity>
              <View style={{ marginLeft: "4%", flex: 1, marginTop: "4%" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: "4%" }} >
                  <View style={{ flexDirection: "row", }} >
                    <Text>{item.item.firstName}</Text>
                    <Text style={{marginLeft:5}}>{item.item.lastName}</Text>
                  </View>
                  <Text>Time</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text>messages Text...</Text>
                </View>
              </View>
            </TouchableOpacity>)
        }}
      >
      </FlatList>
      <Pressable style={{}} onPress={() => setmodalstatus(!modalstatus)}>
        <Modal visible={modalstatus} transparent={true} animationType={"fade"} onRequestClose={() => { Alert.alert('Modal has been closed.'); setmodalstatus(!modalstatus) }} >
          <BlurView
            style={[{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, },]}
            blurType="dark"
            blurAmount={20}
            reducedTransparencyFallbackColor="white"
          />
          <View style={{ backgroundColor: "white", position: "relative", width: "73%", height: "40%", marginTop: "43%", marginLeft: '15%' }}>
            <Icon style={{ position: 'absolute', top: 30, right: 0, color: "black", }} onPress={() => { setmodalstatus(!modalstatus) }} name='cross' size={35}></Icon>
              <Image source={{uri:selectedUserData?.item?.image}} style={{ width: 250, height: 250,position: 'absolute', top:14,right:30,}} resizeMode='cover' ></Image>
            <View style={{ height: 35, backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",flexDirection:"row" }}>
              <Text style={{ color: "white", marginLeft: 15 }}>{selectedUserData?.item?.firstName}</Text>
              <Text style={{ color: "white", marginLeft: 8 }}>{selectedUserData?.item?.lastName}</Text>
            </View>
            <View style={{ height: 45, backgroundColor: "white", bottom: -229, justifyContent: "space-between", flexDirection: "row" }}>
              <TouchableOpacity style={{ width: "25%", alignItems: "center", justifyContent: "center" }} >
                <SecondIcon name='android-messages' size={25} color='#006D5B'></SecondIcon>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: "25%", alignItems: "center", justifyContent: "center" }} >
                <ThirdIcon name='call' size={25} color='#006D5B'></ThirdIcon>
              </TouchableOpacity><TouchableOpacity style={{ width: "25%", alignItems: "center", justifyContent: "center" }} >
                <FourthIcon name='video-camera' size={25} color='#006D5B'></FourthIcon>
              </TouchableOpacity><TouchableOpacity style={{ width: "25%", alignItems: "center", justifyContent: "center" }} >
                <FifthIcon name='info' size={25} color='#006D5B'></FifthIcon>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </Pressable>




    </View>
  )
}

export default MainChat