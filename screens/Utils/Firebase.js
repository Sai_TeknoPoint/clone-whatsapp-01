import firebase from '@react-native-firebase/app';

const firebaseConfig = {
    // Replace with your Firebase configuration settings
    apiKey: "AIzaSyBUDXSqkfaxgO2LGiC9me1wBsMEJQTeiI4",
    authDomain: "chatsapp-4c9df.firebaseapp.com",
    projectId: "chatsapp-4c9df",
    storageBucket: "chatsapp-4c9df.appspot.com",
    messagingSenderId: "358228310670",
    appId: "1:358228310670:web:43d415b918bce57cca27df",
    measurementId: "G-5V9ZTF7XCN"
    // ...
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export default firebase;